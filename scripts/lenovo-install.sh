#!/usr/bin/env bash

set -e
set -x

echo "Upgrading packages. This might take a while..."
sudo apt update
sudo apt upgrade -y
sudo apt install pavucontrol
sudo apt autoremove

echo "Setting up Pulseaudio configuration files..."
# Let's add the load-module line at the end of /etc/pulse/default.pa only if not already there
# if grep -q "Xarxa Suport Poble-Sec" /etc/pulse/default.pa ; then
#     printf "\n#Xarxa Suport Poble-Sec\nload-module module-remap-source source_name=record_mono master=alsa_input.pci-0000_00_1f.3.analog-stereo master_channel_map=front-left channel_map=mono" >> /etc/pulse/default.pa
# fi

# Better update the user-space config file
wget -O ~/.config/pulse/default.pa https://gitlab.com/xarxa-poblesec-tech/digital-edu/-/raw/master/files/default.pa

echo "Restarting Pulseaudio...\n"
sudo killall -q 9 pulseaudio || true
sleep 2
pulseaudio --start

echo "Set default desktop to Ubuntu-like\n"
sudo chmod 000 /usr/share/lightdm/lightdm.conf.d/70-gnome-flashback-metacity.conf

echo "Reconfigure apps in app viewer"
wget -qO- https://gitlab.com/xarxa-poblesec-tech/digital-edu/-/raw/master/scripts/gnome-dash-fix/appfixer.sh | bash
